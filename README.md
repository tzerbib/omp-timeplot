# omp-timeplot <!-- omit in toc -->  

- [Description](#description)
- [Screenshots](#screenshots)
- [Dependencies](#dependencies)
- [Usage](#usage)
  - [Chunk size and iteration number](#chunk-size-and-iteration-number)
  - [Options](#options)
  - [Graphical parameters](#graphical-parameters)
- [Author](#author)
- [Licence](#licence)

## Description  

omp-timeplot is a simple tool for benchmarking programs made with openmp
(or others which use the environment variable *OMP_NUM_THREADS*).

This script was made quite quickly during my schooling to help me visualize 
performance of parallized algorithms (with openmp) and non parallized ones.
It is so pretty ugly (you are now warned!).

If you are seeking for a more generic tool, you can take a look
to [hyperfine](https://github.com/sharkdp/hyperfine)


## Screenshots

![](img/perf.png)


## Dependencies

For this script to run, you will need:
- a UNIX like architecture: this script will try to write in /tmp so be sure
  to have the necessary rights.
- *bc* command
- *sort* command
- *gnuplot*: Usefull for drawing graphs.


## Usage

### Chunk size and iteration number

This script distinguishe a chunk (parameter *-c*) and a number of time
to run the program (parameter *-n*).  

The number of runs is basically used to perform $n$ times the computation
of a chunk, getting the best, worst and average real, user and system time for
each chunk.

In fact, the parameter *chunk* is used to avoid rounding error.  
The script runs $c$ times the given software and divide
the total time by the chunk size.

As example, if you try to run a short program and set the chunk size
to a small value, you will have a lot of rounding errors

```bash
# Example with a lot of iterations but a small chunk size
timeplot -n 1000 -c 1 -o 5 -t "" sleep 0.001

> Initial time mesurment (with 1 threads)...  0.000000s
```

For such programs, you will get better result with a higher chunk size.
```bash
# Example with less iterations but a higher chunk size
timeplot -n 10 -c 100 -o 5 -t "" sleep 0.001

> Initial time mesurment (with 1 threads)...  0.002300s
```

But keep in mind that this script will run the program a number of time
equals to $chunk\_size \times number\_of\_runs \times thread\_values$.


### Options

- *-h* (or *-?*)    Display this information.
- *-c \<size\>*     Set the size of a chunk to <size>.
- *-n* *\<runs\>*   Set number of runs for benchmark.
- *-o* *\<output\>* Output mode (0: no output, 1:table, 2:separate graphs, 4:all in one graph).
                    Default is 5
- *-t* *\<list\>*   Change the number of thread used for the benchmark
                    (Example: timeplot -t "2 3 4 5" \<foo.exe\>).
- *-q*              Quiet mode.


### Graphical parameters

By default, this script can create some graphs using gnuplot.
You can change the pattern of these by modifying files with *setup* extension.



## Author

> [Timothée ZERBIB](https://gitlab.com/Franz.Hopper) Student at the
University of Strasbourg in networks and operating systems.


## Licence

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  1. You just DO WHAT THE FUCK YOU WANT TO.
```